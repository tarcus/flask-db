# need names_dataset/first_names.all.txt and names_dataset/last_names.all.txt
# from https://github.com/philipperemy/name-dataset
import os
import random


class NameDataset:
    FIRST_NAME_SEARCH = 'FIRST_NAME_SEARCH'
    LAST_NAME_SEARCH = 'LAST_NAME_SEARCH'

    def __init__(self):
        first_names_filename = os.path.join(os.path.dirname(__file__), 'first_names.all.txt')
        with open(first_names_filename, 'r', errors='ignore', encoding='utf8') as r:
            self.first_names = tuple(r.read().strip().split('\n'))
        last_names_filename = os.path.join(os.path.dirname(__file__), 'last_names.all.txt')
        with open(last_names_filename, 'r', errors='ignore', encoding='utf8') as r:
            self.last_names = tuple(r.read().strip().split('\n'))

    def get_random_name(self, name_type):
        names = self.first_names if name_type == NameDataset.FIRST_NAME_SEARCH else self.last_names
        return names[random.randint(1, 50000)]

    def get_random_first_name(self):
        return self.get_random_name(name_type=NameDataset.FIRST_NAME_SEARCH)

    def get_random_last_name(self):
        return self.get_random_name(name_type=NameDataset.LAST_NAME_SEARCH)

