import random as r
import names
import sqlite3

conn = sqlite3.connect('data.db')

query_create_table_users = '''
    CREATE TABLE users(
        id INTEGER PRIMARY KEY
        ,name VARCHAR(100) NOT NULL
        ,email VARCHAR(100) NOT NULL
        )
'''

query_create_table_full_data = '''
    CREATE TABLE users_data(
        id INTEGER PRIMARY KEY
        ,user_id INTEGER NOT NULL
        ,age INTEGER NOT NULL
        ,phone INTEGER
        ,country VARCHAR(100) NOT NULL
        ,city VARCHAR(100) NOT NULL
        )    
'''

#conn.execute(query_create_table_users)
#conn.execute(query_create_table_full_data)

usernames = names.NameDataset()
countries = ['Russia', 'Uganda', 'USA', 'UK']
cities = ['Moscow', 'Uhwe', 'New-York', 'London']
users = []
users_data = []
for i in range(1,51):
    name = usernames.get_random_first_name()
    lastname = usernames.get_random_last_name()
    email = f'{name}.{lastname}@example.com'
    users.append((name, email))
    country_id = r.randint(0, 3)
    user_data = (i, r.randint(10, 80), r.randint(1000000000, 8999999999), countries[country_id], cities[country_id])
    users_data.append(user_data)

query_add_users = '''
    INSERT INTO users
    (name, email)
    VALUES
    (?,?)
'''

query_add_users_data = '''
    INSERT INTO users_data
    (user_id,age,phone,country,city)
    VALUES
    (?,?,?,?,?)
'''

conn.executemany(query_add_users, users)
conn.executemany(query_add_users_data, users_data)
conn.commit()