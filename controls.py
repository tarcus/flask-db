import sqlite3


class UsersControl():
    def __init__(self, dbname: str = None):
        self.conn = sqlite3.connect(dbname, check_same_thread=False)

    def get_users(self) -> list:
        query = '''
            SELECT * FROM users LIMIT 10
        '''

        result = self.conn.execute(query).fetchall()
        return result

    def get_user_data(self, user_id: int) -> tuple:
        query = lambda uid: f'''
            SELECT * FROM users
            INNER JOIN users_data
            ON users.id = users_data.user_id
            WHERE users.id = {uid}
        '''

        result = self.conn.execute(query(user_id)).fetchone()

        return result

    def add_user(self, user):
        query_users = lambda user: f'''
            INSERT INTO users
            (name, email)
            VALUES
            ('{user['name']}','{user['email']}')
        '''
        cursor = self.conn.execute(query_users(user))
        last_row_id = cursor.lastrowid

        query_users_data = lambda user, user_id: f'''
            INSERT INTO users_data
            (user_id,age,phone,country,city)
            VALUES
            ({user_id},{user['age']},{user['phone'] if user['phone'] else '0'},'{user['country']}','{user['city']}')
        '''
        cursor.execute(query_users_data(user, last_row_id))
        self.conn.commit()
        return last_row_id


if __name__ == '__main__':
    uc = UsersControl('data.db')
    print(uc.get_users())
    print(uc.get_user_data(34))