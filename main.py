import flask as fl
from controls import UsersControl

app = fl.Flask(__name__)
control = UsersControl('data.db')


@app.route('/')
def home():
    return fl.render_template('home.html', page='Home')


@app.route('/users')
def users():
    uid = fl.request.args.get('id')
    if uid:
        user = control.get_user_data(uid)
        return fl.render_template('user.html', user=user, page=f'Userland: {user[1]}')

    return fl.render_template('users.html', users=control.get_users(), page='Users')

@app.route('/about')
def about():
    return fl.render_template('about.html', page='About us')


@app.route('/users/add_user', methods=['GET', 'POST'])
def user_form():
    request = fl.request
    if request.method == 'POST':
        user_id = control.add_user(request.form)
        return fl.redirect(f'/users?id={user_id}')
    return fl.render_template('user_form.html', page='User control')


@app.errorhandler(404)
def not_found(error):
    return fl.render_template('404.html', page='404 NOT FOUND')

if __name__ == '__main__':
    app.run(debug=True)